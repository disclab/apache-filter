package org.littleshoot.proxy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;

/**
 * Convenience base class for implementations of {@link HttpFiltersSource}.
 */
public class HttpFiltersSourceAdapter implements HttpFiltersSource {

    public HttpFilters filterRequest(HttpRequest originalRequest) {
        //Start edits to original code
        //System.err.println("Inside filterRequest()");
        
       // System.err.println(originalRequest.getUri());
        //End edits to original code
        return new HttpFiltersAdapter(originalRequest, null);
    }
    
    @Override
    public HttpFilters filterRequest(HttpRequest originalRequest,
            ChannelHandlerContext ctx) {
        //Start edits to original code
        //System.err.println("Inside filterRequest()");
        //End edits to original code
        return filterRequest(originalRequest);
    }

    @Override
    public int getMaximumRequestBufferSizeInBytes() {
        //Start edits
        return 100048576;
        //return 1048576;
        //return 0;
        //end edits
        
    }

    @Override
    public int getMaximumResponseBufferSizeInBytes() {
        //Start edits
        return 100048576;
        //return 1048576;
        //return 0;
        //end edits
    }

}
