package edu.cmich.mobileproxy.wifiman;

import java.net.InetSocketAddress;
import edu.cmich.mobileproxy.Constants;
import static edu.cmich.mobileproxy.LogUtils.LOGD;

public class ServerThread extends Thread {
    public boolean keepRunning = true;
    private static String TAG = "AsyncWebSocketServerTask";

    public ServerThread() {

    }

    public void run() {
        LOGD(TAG, "Starting server");
        try {
            InetSocketAddress isa = new InetSocketAddress(Constants.SERVER_PORT);
            WSServerClass wss = new WSServerClass(isa);
            wss.start();
            LOGD(TAG, "Server running at " + wss.getAddress().toString());
        } catch (Exception e) {
            LOGD(TAG, "Error starting server");
            e.printStackTrace();
        }
        while (keepRunning) {
            try {
                Thread.sleep(10000);
                //LOGD(TAG, "DEBUG ME MORE!");
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}