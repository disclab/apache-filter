/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy.wifiman;

import edu.cmich.mobileproxy.Constants;
import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;
import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.PassableWebObject;
import edu.cmich.mobileproxy.Utils;
import edu.cmich.mobileproxy.decisionman.DecisionMakerMan;
import java.io.File;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

/**
 * @author pseeling
 */
public class WSServerClass extends WebSocketServer {
    private static String TAG = "WSServerClass";

    public WSServerClass(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        LOGD(TAG,"Connected client: " + conn.getResourceDescriptor());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        LOGD(TAG,"onClose reason: " + reason);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        LOGD(TAG, "String message received: " + message);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer bytes) {
        LOGD(TAG, "ByteBuffer message received");
        PassableWebObject pwo = null;

        pwo = Utils.convertByteBufferToPWO(bytes);
        if (pwo != null) {
               LOGD(TAG, "Action received: " + pwo.getAction());
            if(pwo.getAction().equals(Constants.REQUEST_PUSH_RESPONSE)){
                //Mobile device has requested to push its contents, instead, we want
                //this desktop to push it's content to the mobile device
                
                PassableWebObject returnPwo = new PassableWebObject();
                returnPwo.setAction(Constants.REQUEST_PUSH_RESPONSE);
                conn.send(Utils.convertPWOToByteArray(returnPwo));
            }

            //TODO: In regards to comment below, no long have this problem since using different library, move rest of decisions making back into DecisionMan?
            //Handle this here because I need to perform multiple sends
            //and when I was trying to send from within the DecisionMaker class, errors were occurring.
            else if (pwo.getAction().equals(Constants.SEND_LIST_NEEDED_ITEMS)) {

                //Send each requested item to requesting device
                for (String requestedItem : pwo.getNeededItems()) {
                    LOGD(TAG, "Item requested: " + requestedItem);

                    PassableWebObject returnPwo = new PassableWebObject();
                    returnPwo.setAction(Constants.SEND_INDIVIDUAL_RESPONSE);

                    //Attach file to request
                    File attachedFile = new File(ObjectMap.cacheFilePath + "/" + requestedItem);
                    if (!attachedFile.exists()) {
                        LOGE(TAG, "File doesn't exist: " + attachedFile.getAbsolutePath());
                    }
                    returnPwo.setSha(requestedItem);
                    returnPwo.setRequestContents(attachedFile);

                    returnPwo.setWo(ObjectMap.map.get(requestedItem));
                    //Store sha of file (file name) in field to use as name in receiving device
                    LOGE(TAG, "Sending file: " + requestedItem);
                    conn.send(Utils.convertPWOToByteArray(returnPwo));

                }

                //Let receiver know communications are done so they can close down communication methods
                PassableWebObject returnPwo = new PassableWebObject();
                returnPwo.setAction(Constants.LAST_SEND_INDIVIDUAL_RESPONSE);
                conn.send(Utils.convertPWOToByteArray(returnPwo));

                //TODO: Need to load updated object map?
                ObjectMap.saveObjectMap();
                ObjectMap.loadObjectMap();

                //closeServer();
            } else if (pwo.getAction().equals(Constants.LAST_SEND_INDIVIDUAL_RESPONSE)) {
                LOGD(TAG, "LAST_SEND_INDIVIDUAL_RESPONSE");
                //TODO: Shutdown server after communications are done or leave running in background?
                //Shut down server since no more communications are expected
                //closeServer();
            } else {
                PassableWebObject returnPwo = DecisionMakerMan.makeDecision(pwo);
                if (returnPwo != null) {
                    conn.send(Utils.convertPWOToByteArray(returnPwo));
                } else {
                    //Item received, no need to response with anything back to sender
                    LOGD(TAG, "Returnpwo is null, not sending anything back");
                }
            }
        } else {
            LOGD(TAG, "Pwo is null!");
        }   
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        System.err.println("An error occured:" + ex);
        System.err.println("Connection " + conn.getResourceDescriptor());
    }

}