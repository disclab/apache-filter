/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import static edu.cmich.mobileproxy.Desktop_Proxy.addresses;
import static edu.cmich.mobileproxy.LogUtils.LOGD;
import edu.cmich.mobileproxy.cacheman.CacheService;
import edu.cmich.mobileproxy.proxyman.ProxyControl;
import edu.cmich.mobileproxy.wifiman.ServerThread;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

/**
 *
 * @author pseeling
 */
public class Desktop_Proxy {

    // Event Bus
    private static Bus BUS = new Bus(ThreadEnforcer.ANY);

    private static Preferences preferences = Preferences.userRoot().node("CacheConnect");
    private final static String TAG = "MAIN";
    public static String DEFAULT_STORAGE_LOCATION;
    static Set<String> addresses;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO: Double-check if this works...
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                LOGD(TAG, "Shutdown hook called");
                preferences.putLong(Constants.REQUESTS_SAVED_KEY, ObjectMap.totalRequestsSaved);
                preferences.putLong(Constants.BYTES_SAVED_KEY, ObjectMap.totalBytesSaved);
            }
        }));

        DEFAULT_STORAGE_LOCATION = System.getProperty("user.dir") + File.separator + "CacheProxy" + File.separator;
        LOGD(TAG,"Default storage location: " + DEFAULT_STORAGE_LOCATION);

        // TODO: remove after debugging.
        clearCache();

        preferences.putByteArray(Constants.STORAGE_KEY, DEFAULT_STORAGE_LOCATION.getBytes());
        String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, DEFAULT_STORAGE_LOCATION.getBytes()));
        
        addresses = new HashSet<>();
        byte[] bytesFromPreferences = preferences.getByteArray(Constants.ADDRESSES_KEY, null);
        if (bytesFromPreferences != null) {
            try {
                ByteArrayInputStream bis = new ByteArrayInputStream(bytesFromPreferences);
                ObjectInput in = new ObjectInputStream(bis);
                addresses = (HashSet<String>) in.readObject();
                
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(Desktop_Proxy.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(Desktop_Proxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (addresses
                != null) {
            LOGD(TAG, "ADDRESSES_KEY already set.");
        } else {
            try {
                ByteArrayOutputStream bos = null;
                LOGD(TAG, "ADDRESSES_KEY not set yet, setting to empty string set");
                addresses = new HashSet<>();
                bos = new ByteArrayOutputStream();
                ObjectOutput out = new ObjectOutputStream(bos);
                out.writeObject(addresses);
                preferences.putByteArray(Constants.ADDRESSES_KEY, bos.toByteArray());
                bos.close();
            } catch (IOException ex) {
                Logger.getLogger(Desktop_Proxy.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        ObjectMap.totalRequestsSaved = preferences.getLong(Constants.REQUESTS_SAVED_KEY, 0L);
        ObjectMap.totalBytesSaved = preferences.getLong(Constants.BYTES_SAVED_KEY, 0L);

        //Start cache service
        Thread cacheController = new CacheService();
        cacheController.start();

        //Start proxy services
        Thread proxyController = new ProxyControl();
        proxyController.start();
        
        //Start server
        Thread server = new ServerThread();
        server.start();

        (new Scanner(System.in)).nextLine();
        System.exit(0);
    }

    public static Bus getBusInstance() {
        return BUS;
    }

    public static void clearCache() {
        File cacheDir = new File(DEFAULT_STORAGE_LOCATION);

        for (File f : cacheDir.listFiles()) {
            if (f.isFile()) {
                f.delete();
            } else {
                for (File cacheFile : f.listFiles()) {
                    cacheFile.delete();
                }
            }
        }
    }
}
