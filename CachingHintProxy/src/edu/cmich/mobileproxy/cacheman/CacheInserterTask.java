package edu.cmich.mobileproxy.cacheman;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.PassableWebObject;

import static edu.cmich.mobileproxy.LogUtils.LOGD;

/**
 * Created by johns4ta on 9/24/2015.
 */
//Inserts the webobject contained in the PassableWebObject into ObjectMap and copied request onto disk
public class CacheInserterTask extends Thread{

    private static String TAG = "CacheInserterTask";
    PassableWebObject pwo = null;

    public CacheInserterTask(PassableWebObject pwobject) {
        this.pwo = pwobject;
    }

    public void run() {

        byte[] requestData = pwo.getRequestContents();

        try {
            // TODO: File checking to make sure file doesn't exist already
            FileUtils.writeByteArrayToFile(new File(ObjectMap.cacheFilePath, pwo.getSha()), requestData);
            LOGD(TAG, "Copied file to disk");
        } catch (IOException e) {
            LOGD(TAG, "Error writing file to disk");
            e.printStackTrace();
        }

        try {
            ObjectMap.map.put(pwo.getSha(), pwo.getWo());
            LOGD(TAG, "Placed item into map");
        } catch (Exception e) {
            LOGD(TAG, "Problem placing new item into map");
            e.printStackTrace();
        }
    }

}
