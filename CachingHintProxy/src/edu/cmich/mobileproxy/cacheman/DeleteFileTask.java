package edu.cmich.mobileproxy.cacheman;

/**
 * Created by johns4ta on 9/24/2015.
 */
import java.io.File;

import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;

public class DeleteFileTask extends Thread{

    private static String TAG = "DeleteFileTask";
    private static File file; 
    public DeleteFileTask(File file) {
        this.file = file;
    }
    
    

   public void run() {
        try {
            file.delete();
            LOGD(TAG, "Succesfully deleted file " + file.getAbsolutePath());
        } catch (Exception e) {
            LOGE(TAG, "Error deleting " + file.getAbsolutePath());
            e.printStackTrace();
        }
    }


}
