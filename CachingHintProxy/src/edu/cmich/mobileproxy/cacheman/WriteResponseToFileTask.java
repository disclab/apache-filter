package edu.cmich.mobileproxy.cacheman;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.WebObject;
import edu.cmich.mobileproxy.pssha1;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponse;
import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;

/**
 * Created by johns4ta on 9/16/2015.
 */
public class WriteResponseToFileTask extends Thread {

    private static String TAG = "WriteResponseToFile";
    HttpObject httpObject;
    ByteArrayOutputStream response;
    String responseName, sha1;
    File storage;

    //Typical format of date string: Sun, 13 Sep 2015 22:50:01 GMT
    DateFormat expiresFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

    public WriteResponseToFileTask(HttpObject httpObject, ByteArrayOutputStream response, String responseName, String sha1, File storage) {
        this.httpObject = httpObject;
        this.response = response;
        this.responseName = responseName;
        this.sha1 = sha1;
        this.storage = storage;
    }

    public void run() {
        System.out.println("Running " + sha1);
        long receiveTime = System.currentTimeMillis();
        boolean expirationWasSet = false;

        // Add to map
        WebObject wO = new WebObject();
        List<Map.Entry<String, String>> hentries = ((HttpResponse) httpObject).headers().entries();
        for (Map.Entry<String, String> element : hentries) {
            if (element.getKey().equalsIgnoreCase("Content-Type")) {
                System.out.println("MIME type: " + element.getValue());
                wO.setMIME_TYPE(element.getValue());
            } else if (element.getKey().equalsIgnoreCase("Cache-Control")) {
                // Examples: Cache-Control: max-age=3600, must-revalidate Expires: Fri, 30 Oct 1998 14:19:41 GMT
                String[] values = element.getValue().toLowerCase().replaceAll("\\s", "").split(",");
                for (String value : values) {
                    if (value.contains("max-age")) {

                        try {
                            long expirationTime = Long.parseLong(value.split("=")[1]);
                            wO.setExpiration(receiveTime + expirationTime * 1000);
                            expirationWasSet = true;
                        } catch (Exception e) {
                            //TODO: Insert a default timestamp if time parsing fails??
                            LOGE(TAG, "Error converting max-age " + element.getValue());
                        }
                        break;
                    }

                }
            } else if ((element.getKey().equalsIgnoreCase("expires"))) { // alternative,
                try {
                    //Typical format of date string: Sun, 13 Sep 2015 22:50:01 GMT
                    Date date = expiresFormatter.parse(element.getValue());

                    // TODO: cleanup, was this static?
                    wO.setExpiration(date.getTime());
                    expirationWasSet = true;
                } catch (Exception e) {
                    //TODO: Insert a default timestamp if time parsing fails??
                    LOGE(TAG, "Error formatting date: " + element.getValue());
                }
            }
        }

        if (!wO.getMIME_TYPE().equals("text/html")) {
            wO.setContentHash(pssha1.SHAsum(response.toByteArray()));
        }

        ObjectMap.map.put(sha1, wO);

        if (!fileExistsInCache(sha1)) {
            try {
                OutputStream outputStream = new FileOutputStream(
                        new File(storage.getAbsolutePath() + "/" + sha1));
                try {
                    response.writeTo(outputStream);
                    outputStream.close();
                } catch (IOException ex) {
                    LOGE(TAG, "Error writing file to cache.");
                    LOGE(TAG, ex.toString());
                }
            } catch (FileNotFoundException ex) {
                LOGE(TAG, "Unable to write file to cache location. Sha1: " + sha1);
            }
        } else {
            LOGD(TAG, "File already exists in cache location. Response Name: " + responseName + " sha1: " + sha1);
        }
    }

    public boolean fileExistsInCache(String sha1) {
        File myFile;
        return (myFile = new File(storage.getAbsolutePath() + "/" + sha1)).exists();
    }
}
