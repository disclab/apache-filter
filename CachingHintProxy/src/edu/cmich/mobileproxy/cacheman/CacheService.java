package edu.cmich.mobileproxy.cacheman;

import com.squareup.otto.Bus;
import edu.cmich.mobileproxy.Desktop_Proxy;
import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.WebObject;

import static edu.cmich.mobileproxy.LogUtils.*;

public class CacheService extends Thread {

    private static boolean isRunning = false;
    private final String TAG = "CacheService";
    private CacheServiceIncomingHandler incomingHandler;
    private ObjectMap mApplication;
    private static Timer timer = new Timer();
    Bus bus;

    public CacheService() {

    }

    public static boolean isRunning() {
        return isRunning;
    }

    @Override
    public void run() {
        LOGD(TAG, "Start");
        // Register with the event bus
        bus = Desktop_Proxy.getBusInstance();
        bus.register(this);

        mApplication.initCacheFilePath();

        // TODO: Double-check if this works...
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                LOGD(TAG, "Shutdown hook called");
                ObjectMap.saveObjectMap();
            }
        }));
        ObjectMap.loadObjectMap();

        LOGD(TAG, "Service Started.");
        isRunning = true;

        //TODO: Decide on what the interval should be for checking expired cache contents
        //Delay, Interval
        timer.scheduleAtFixedRate(new onTimerTick(), 6000, 30000);
    }

    private class onTimerTick extends TimerTask {

        public void run() {
            LOGD(TAG, "Timer executed");
            //cleanExpiredCacheContents();
            ObjectMap.saveObjectMap();
        }
    }

    //Deletes expired items from cache
    public void cleanExpiredCacheContents() {
        ObjectMap.saveObjectMap();

        Iterator it = ObjectMap.map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String sha = pair.getKey().toString();
            WebObject wo = (WebObject) pair.getValue();

            long curTime = System.currentTimeMillis();
            if (wo.getExpiration() < curTime) {

                File myFile = new File(ObjectMap.cacheFilePath.getAbsolutePath() + "/" + sha);
                new DeleteFileTask(myFile).start();

                if (ObjectMap.deleteFileFromMap(sha)) {
                    LOGD(TAG, "Succesfully deleted file " + sha + " from map");
                } else {
                    LOGD(TAG, "Error deleting file " + sha + " from map");
                }
            }
        }
        ObjectMap.saveObjectMap();
    }

    /*
    public void onDestroy() {
        super.onDestroy();

        try {
            // doUnbindService();
        } catch (Throwable t) {
            LOGE(TAG, "Failed to unbind from the service");
        }
        isRunning = false;

        // Self-restarting
        Intent intent = new Intent("edu.cmich.mobileproxy.cacheman.restart");
        LOGD(TAG, "Restarting myself.");
        LocalBroadcastManager.getInstance(this.getApplicationContext()).sendBroadcast(intent);
    }
     */
}
