package edu.cmich.mobileproxy.cacheman;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import edu.cmich.mobileproxy.CacheServiceMessage;
import edu.cmich.mobileproxy.Constants;
import edu.cmich.mobileproxy.Desktop_Proxy;
import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;
import static edu.cmich.mobileproxy.LogUtils.LOGI;
import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.PassableWebObject;
import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.prefs.Preferences;

/**
 * Created by pseeling on 9/5/15.
 */
public class CacheServiceIncomingHandler {

    public static final int MSG_CHECK_SHA = 1;
    public static final int MSG_INSERT_FILE = 2;
    public static final int MSG_DELETE_FILE = 3;
    public static final int MSG_REGISTER_CLIENT = 4;
    public static final int MSG_SET_INT_VALUE = 5;
    public static final int MSG_CHECK_SHA_FILE = 6;
    public static final int MSG_UNREGISTER_CLIENT = 7;
    public static final int MSG_PURGE_CACHE = 8;
    public static final int MSG_CHANGE_CACHE_PATH = 9;
    public static final int MSG_GET_NEEDED_CACHE_ITEMS = 10;

    private static String TAG = "CacheServiceIncomingHandler";
    private File cacheFilePath;
    private static Preferences preferences = Preferences.userRoot().node("CacheConnect");

    private Bus bus;

    public CacheServiceIncomingHandler() {
        String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes()));
        if (storagePath == null) {

            LOGD(TAG, "Storage Path is empty!!");
        } else {
            LOGD(TAG, storagePath);
            File storage = new File(storagePath);
            storage.mkdir();
            cacheFilePath = new File(storage.getAbsolutePath(), Constants.CACHE_NAME);
            cacheFilePath.mkdir();
        }
        // Register with the event bus
        bus = Desktop_Proxy.getBusInstance();
        bus.register(this);
    }

    @Subscribe
    public void handleMessage(CacheServiceMessage msg) {
        if (msg.getDestination().equalsIgnoreCase(TAG)) {

            LOGD(TAG, "handleMessage Reached. msg.action contents: " + msg.getAction());

            switch (msg.getAction()) {
                // TODO: Finish this if needed
                case MSG_CHECK_SHA:
                    break;
                case MSG_INSERT_FILE:
                    PassableWebObject pwo = (PassableWebObject) msg.getBody();
                    new CacheInserterTask(pwo).start();
                    LOGD(TAG, "Message received to ADD file.");
                    break;
                // TODO: Finish this if needed
                case MSG_DELETE_FILE:
                    LOGI(TAG, "Message received to DELETE file.");
                    break;
                // TODO: Finish this if needed
                case MSG_SET_INT_VALUE:
                    LOGD(TAG, "MSG_SET_INT_VALUE " + Integer.parseInt(msg.getMessage()));
                    break;
                case MSG_CHECK_SHA_FILE:
                    String sha = msg.getMessage();
                    LOGD(TAG, "SHA1: " + sha);
                    if (sha != null) {
                        CacheServiceMessage csmsg = new CacheServiceMessage();
                        csmsg.setDestination(msg.getSender());
                        csmsg.setSender(TAG);
                        csmsg.setAction(MSG_CHECK_SHA_FILE);
                        csmsg.setMessage(sha);
                        csmsg.setBody(new Boolean(fileExistsInCache(sha)));
                        bus.post(csmsg);
                    } else {
                        LOGD(TAG, "SHA is null.");
                    }
                    break;
                case MSG_PURGE_CACHE:
                    LOGD(TAG, "Message received to purge cache.");
                    purgeCache();
                    break;
                case MSG_CHANGE_CACHE_PATH:
                    String newPath = msg.getMessage();
                    LOGD(TAG, "Message received to change cache path to " + newPath);
                    changeCachePath(newPath);
                    break;
                case MSG_GET_NEEDED_CACHE_ITEMS:
                    LOGD(TAG, "THIS SHOULDN'T HAVE BEEN REACHED, CODE FOR GETTING NEED CACHE ITEMS HAS BEEN MOVED TO UTILS.");
                    LOGD(TAG, "Message received to get list of needed cache items");
                    break;
            }
        }
    }

    public boolean changeCachePath(String newPath) {
        try {
            preferences.putByteArray(Constants.STORAGE_KEY, newPath.getBytes());
            ObjectMap.initCacheFilePath();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            LOGE(TAG, "Error changing cache path.");
            return false;
        }
    }

    public boolean fileExistsInCache(String sha1) {
        File myFile;
        return (myFile = new File(cacheFilePath.getAbsolutePath() + "/" + sha1)).exists();
    }

    public boolean purgeCache() {

        try {
            Iterator it = ObjectMap.map.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                String sha = pair.getKey().toString();

                File myFile = new File(cacheFilePath.getAbsolutePath() + "/" + sha);
                new DeleteFileTask(myFile).start();
                if (ObjectMap.deleteFileFromMap(sha)) {
                    LOGD(TAG, "Succesfully deleted file " + sha + " from map");
                } else {
                    LOGD(TAG, "Error deleting file " + sha + " from map");
                }
            }

            ObjectMap.saveObjectMap(); //refresh contents of maps.ser file

            //Delete any files that may not have had references in the .ser file
            String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes()));
            if (storagePath != null) {
                File storage = new File((String) storagePath, Constants.CACHE_NAME);
                //File storage = new File((String) Hawk.get(Constants.STORAGE_KEY), Constants.CACHE_NAME);
                try {
                    File[] files = storage.listFiles();
                    if (files != null) {
                        for (File f : files) {
                            // TODO: Change to parallel execution?
                            new DeleteFileTask(f).start();
                        }
                    }
                } catch (Exception e) {
                    LOGE(TAG, "Error purging cache");
                    e.printStackTrace();
                }

                return true;
            } else {
                LOGE(TAG, "storagePath is null");
                return false;
            }
        } catch (Exception e) {
            LOGE(TAG, "Error purging cache");
            e.printStackTrace();
            return false;
        }
    }
}
