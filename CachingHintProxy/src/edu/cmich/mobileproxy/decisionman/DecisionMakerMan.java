package edu.cmich.mobileproxy.decisionman;

import edu.cmich.mobileproxy.Constants;
import edu.cmich.mobileproxy.Desktop_Proxy;
import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;
import edu.cmich.mobileproxy.PassableWebObject;
import edu.cmich.mobileproxy.Utils;
import edu.cmich.mobileproxy.cacheman.CacheInserterTask;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;

/**
 * Created by johns4ta on 10/16/2015.
 */
public class DecisionMakerMan {

    private static String TAG = "DecisionMakerMan";

    //Makes the decision on what to do next when a passable web object has been received
    //based off the action field contained in the passable web object
    public static PassableWebObject makeDecision(PassableWebObject pwo) {

        Preferences preferences = Preferences.userRoot().node("CacheConnect");
        PassableWebObject newPwo = new PassableWebObject();

        switch (pwo.getAction()) {

            //Request from other device to push other device's cache items to this device
            case Constants.REQUEST_PUSH_RESPONSE:
                LOGD(TAG, "Action received: REQUEST_PUSH_RESPONSE");
                //Request SER File to parse out what this device needs that other device has
                newPwo.setAction(Constants.REQUEST_SER_FILE);
                break;

            //User requested SER File, send user back ser file
            case Constants.REQUEST_SER_FILE:
                LOGD(TAG, "Action received: REQUEST_SER_FILE");
                newPwo.setAction(Constants.SEND_SER_FILE);
                //Attach SERFILE to pwo
                
               // String defaultLocation = System.getProperty("user.dir") + File.separator + "CacheProxy" + File.separator;
                newPwo.setRequestContents(new File(new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes())), "maps.ser"));
                break;

            //User has received SER file from another user, parse file and determine needed items
            //and send list of needed items back to sender
            case Constants.SEND_SER_FILE:
                LOGD(TAG, "Action received: SEND_SER_FILE");

                File incomingSer = new File(new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes())), "incomingMaps.ser");

                //If file doesn't exist, force it to make an empty file
                if (!incomingSer.exists()) {
                    try {
                        incomingSer.createNewFile();
                    } catch (IOException e) {
                        LOGE(TAG, "Error creating new file");
                        e.printStackTrace();
                    }
                }

                //Write contents of byte array to file so the ser file can be read from
                if (Utils.writesByteArrayToDisk(incomingSer, pwo.getRequestContents())) {
                    LOGD(TAG, "Successfully wrote incoming SER file to disk.");
                } else {
                    LOGD(TAG, "Error writing incoming SER file to disk.");
                }

                ArrayList<String> neededItems = Utils.determineNeededCacheItems(incomingSer.getAbsolutePath());

                newPwo.setAction(Constants.SEND_LIST_NEEDED_ITEMS);
                newPwo.setNeededItems(neededItems);
                break;

            //User has received list of needed items from other user
            //Currently handled in calling class (WebSockerServer & WebSocketClient)
            case Constants.SEND_LIST_NEEDED_ITEMS:
                LOGD(TAG, "Action received: SEND_LIST_NEEDED_ITEMS");
                break;

            //User has received an individual response from user
            case Constants.SEND_INDIVIDUAL_RESPONSE:
                LOGD(TAG, "Action received: SEND_INDIVIDUAL_RESPONSE");
                LOGD(TAG, "Item received: " + pwo.getSha());
                //Execute async task to insert pwo into hashmap and copy file to disk
                new CacheInserterTask(pwo).start();

                newPwo = null; //Don't want to send any response back
                break;

            //User has received LAST individual response from user
            //Currently handled in calling class (WebSockerServer & WebSocketClient)
            case Constants.LAST_SEND_INDIVIDUAL_RESPONSE:
                LOGD(TAG, "Action received: LAST_SEND_INDIVIDUAL_RESPONSE");
                break;
        }
        return newPwo;
    }
}
