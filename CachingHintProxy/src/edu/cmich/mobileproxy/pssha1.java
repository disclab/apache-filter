/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

/**
 * @author pseeling
 */
public class pssha1 {
    public static String SHAsum(byte[] convertme) { //throws NoSuchAlgorithmException {
        return com.google.common.hash.Hashing.sha1().hashBytes(convertme).toString();
//        MessageDigest md = MessageDigest.getInstance("SHA-1");
//        return byteArray2Hex(md.digest(convertme));
    }

    private static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }
}
