package edu.cmich.mobileproxy;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import static edu.cmich.mobileproxy.LogUtils.LOGE;

/**
 * Created by johns4ta on 9/22/2015.
 */
public class PassableWebObject implements Serializable {

    private String action;
    private String TAG = "PassableWebObject";
    private byte[] requestContents;
    private String sha;
    private WebObject wo;
    private ArrayList<String> neededItems;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public WebObject getWo() {
        return wo;
    }

    public void setWo(WebObject wo) {
        this.wo = wo;
    }

    public PassableWebObject() {

    }

    public PassableWebObject(String sha, WebObject wo, String fullFilePath) {
        super();
        this.sha = sha;
        this.wo = wo;
        setRequestContents(new File(fullFilePath));
    }

    public String getSha() {
        return this.sha;
    }

    public void setSha(String sha) {
        this.sha = sha;
    }

    public byte[] getRequestContents() {
        return requestContents;
    }

    public void setNeededItems(ArrayList<String> neededItems) {
        this.neededItems = neededItems;
    }

    public ArrayList<String> getNeededItems() {
        return neededItems;
    }

    //TODO: Change return type to boolean if fails?
    public void setRequestContents(File file) {
        try {
            requestContents = FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            LOGE(TAG, "Error setting requests contents");
            e.printStackTrace();
        }
    }
}
