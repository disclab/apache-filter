package edu.cmich.mobileproxy.caching_hint;

public class HashedResourceHtmlTag {
    public String tag;
    public String tagHtml;
    public int startIndex;
    public int endIndex;
    public String resourceValue;
    public String uriHash;
    public String contentHash;

    @Override
    public String toString() {
        return tagHtml;
    }
}
