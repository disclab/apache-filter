package edu.cmich.mobileproxy.caching_hint;

import java.text.*;
import java.util.*;

public class HtmlFilter {
    /**
     * Represents the set of HTML tags that embed content from external resources.
     */
    private static class EmbeddedTags {
        private static final String[] _tags = new String[]
                {"script", "link", "img", "iframe", "embed", "object", "video", "audio", "source", "track", "area"};
        private static final String[] _attrs = new String[]
                {"src", "href", "src", "src", "src", "data", "src", "src", "src", "src", "href"};

        /**
         * Gets the name of the attribute that references external resources in the specified HTML tag type.
         * @param tagName The type of tag for which to fetch the attribute name.
         * @return The attribute name that references external resources if the tag can embed content; otherwise, null.
         */
        public static String getAttributeName(String tagName) {
            int index = -1;

            // Find the tag in the array of tag types.
            for (int i = 0; i < _tags.length; i++) {
                if (_tags[i].equals(tagName)) {
                    index = i;
                    break;
                }
            }

            // Return the associated attribute if found.
            if (index != -1) {
                return _attrs[index];
            } else {
                return null;
            }
        }

        /**
         * Returns a value indicating whether the specified tag type can refer to external resources.
         * @param tagName The tag to check.
         * @return True if the specified type of tag can refer to an external resource; otherwise, false.
         */
        public static boolean isEmbeddedTag(String tagName) {
            int index = -1;

            // Find the tag in the array of tag types.
            for (int i = 0; i < _tags.length; i++) {
                if (_tags[i].equals(tagName)) {
                    index = i;
                    break;
                }
            }

            return index != -1;
        }
    }

    /**
     * Extracts and returns the HTML tags from the specified document that both
     * <ol>
     *     <li>link to an external resource, and</li>
     *     <li>contain the specified custom attribute.</li>
     * </ol>
     * @param htmlDocument The document from which to extract the tags.
     * @param customAttribute The custom attribute for which to retrieve the values.
     * @return The list of tags that satisfy both conditions above.
     * @throws ParseException If the document contains malformed HTML.
     */
    public static HashedResourceHtmlTag[] extractResourceTags(String htmlDocument, String customAttribute) throws ParseException {
        List<HashedResourceHtmlTag> documentTags = getDocumentTags(htmlDocument);
        List<HashedResourceHtmlTag> resourceTags = new ArrayList<>();

        // Go through all the tags and discard those that don't link to external resources.
        for (HashedResourceHtmlTag tag : documentTags) {
            // Ignore closing tags, malformed HTML, and tags to don't contain external content.
            if (tag.tag != null && tag.tag.charAt(0) != '/' && EmbeddedTags.isEmbeddedTag(tag.tag)) {
                // Get resource attribute value.
                tag.resourceValue = getAttributeValue(tag, EmbeddedTags.getAttributeName(tag.tag));
                String hash = getAttributeValue(tag, customAttribute);

                if (hash != null) {
                    String[] hashes = getAttributeValue(tag, customAttribute).split(":");
                    tag.uriHash = hashes[0];
                    tag.contentHash = hashes[1];
                }

                if (tag.resourceValue != null && tag.uriHash != null) {
                    resourceTags.add(tag);
                }
            }
        }

        documentTags = null;  // Release resources.
        return resourceTags.toArray(new HashedResourceHtmlTag[0]);
    }

    /**
     * Enumerates a collection of tags and standardizes the URLs to the linked external resources so that the
     * resource URLs give a path relative to the Web server's root.
     * @param tags The collection of tags to enumerate.
     * @param documentUrl The absolute URL for the HTML document that contains the tags.
     * @return A new collection of tags with standardized resource URLs, excluding any tags that did not
     * have a resource URL value.
     */
    public static HashedResourceHtmlTag[] standardizeSourceLinks(HashedResourceHtmlTag[] tags, String documentUrl) {
        // Go through each tag and clean up the path.
        for (HashedResourceHtmlTag tag : tags) {
            if (tag.resourceValue != null) {
                tag.resourceValue = getAbsolutePath(documentUrl, tag.resourceValue);
            }
        }

        return tags;
    }

    /**
     * Gets the absolute path for the specified resource that is referenced by the specified page.
     * @param pageUrl The URL of the document referencing the resource.
     * @param resourceUrl The resource URL as referenced in the document.
     * @return The absolute URL for the resource based on the document URL.
     */
    private static String getAbsolutePath(String pageUrl, String resourceUrl) {
        if (resourceUrl.contains("://")) {
            return resourceUrl;  // Already an absolute path.
        } else {
            int protocolStop = pageUrl.indexOf(':') + 3;
            int pageStart = pageUrl.lastIndexOf('/');

            String protocol = pageUrl.substring(0, protocolStop);
            String baseDir = pageUrl.substring(protocolStop, pageStart);
            String[] baseDirParts = baseDir.split("/");
            StringBuilder newUrl = new StringBuilder();

            newUrl.append(protocol);

            if (resourceUrl.startsWith("/")) {  // Reference is relative to server root.
                newUrl.append(baseDirParts[0]);  // Hostname (root).
                newUrl.append(resourceUrl);
            } else {
                String[] resDirParts = resourceUrl.split("/");
                int endIndex = baseDirParts.length - 1;  // Start at end of path.
                int startIndex = 0;  // Start at beginning of path.

                // Determine whether the paths overlap.
                while (startIndex < resDirParts.length - 1) {  // Ignore resource file name.
                    if (resDirParts[startIndex].equals("..")) {
                        endIndex--;
                    } else if (!resDirParts[startIndex].equals(".")) {
                        break;
                    }

                    startIndex++;
                }

                // Make sure the relative path doesn't refer to a path above the host root.
                if (endIndex < 0) {
                    throw new ArrayIndexOutOfBoundsException("Cannot traverse above server root.");
                }

                // Build the host portion of the absolute path.
                for (int i = 0; i <= endIndex; i++) {
                    newUrl.append(baseDirParts[i]);
                    newUrl.append("/");
                }

                // Build the resource portion of the absolute path.
                while (startIndex < resDirParts.length - 1) {
                    newUrl.append(resDirParts[startIndex++]);
                    newUrl.append("/");
                }

                newUrl.append(resDirParts[startIndex]);  // Add the file name with no trailing slash.
            }

            return newUrl.toString();
        }
    }

    /**
     * Searches the specified tag for the specified attribute and returns that attribute's value, if found.
     * @param tag The HTML tag to search.
     * @param attributeName The attribute for which to retrieve the value.
     * @return The attribute's value, if found; otherwise, an empty string.
     */
    private static String getAttributeValue(HashedResourceHtmlTag tag, String attributeName) {
        int attrIndex = tag.tagHtml.indexOf(attributeName);
        StringBuilder value = null;

        // Start at the index found by the indexOf method call and get the value.  The first double quote
        // encountered is the start and the next double quote is the end.
        if (attrIndex != -1) {
            for (int i = attrIndex; i < tag.tagHtml.length(); i++) {
                char c = tag.tagHtml.charAt(i);

                if (value == null) {
                    if (c == '"') value = new StringBuilder();
                } else {
                    if (c == '"') break;
                    else value.append(c);
                }
            }
        }

        return value == null ? null : value.toString();
    }

    /**
     * Gets a list of HTML tags included in the specified HTML document.
     * @param htmlDocument The document to parse.
     * @return A list of tags in the document.
     * @throws ParseException If the specified document contains invalid or malformed markup.
     */
    private static List<HashedResourceHtmlTag> getDocumentTags(String htmlDocument) throws ParseException {
        List<HashedResourceHtmlTag> tags = new ArrayList<>();
        HashedResourceHtmlTag currentTag = null;

        // Go through document 1 character at a time and extract the HTML tags.
        for (int i = 0; i < htmlDocument.length(); i++) {
            char c = htmlDocument.charAt(i);

            if (c == '<') { // Found a tag, record the start.
                if (currentTag != null) throw new ParseException("Invalid HTML format.", i);

                currentTag = new HashedResourceHtmlTag();
                currentTag.startIndex = i;
            } else if (c == '>') { // Found the end, record it and get the HTML.
                if (currentTag == null) throw new ParseException("Invalid HTML format.", i);

                currentTag.endIndex = i;
                currentTag.tagHtml = htmlDocument.substring(currentTag.startIndex, currentTag.endIndex + 1);
                currentTag.tag = getTagTypeFromHtml(currentTag.tagHtml);

                tags.add(currentTag);
                currentTag = null;
            }
        }

        return tags;
    }

    /**
     * Gets the tag type from the specified HTML tag.
     * @param html The HTML from which to extract the tag type.
     * @return The type of HTML tag represented by the specified markup.
     * @throws IllegalArgumentException If the specified HTML is zero-length.
     */
    private static String getTagTypeFromHtml(String html) throws IllegalArgumentException {
        if (html.length() == 0)
            throw new IllegalArgumentException("Cannot parse zero-length string.");

        if (html.charAt(0) != '<' || html.charAt(html.length() - 1 ) != '>')
            throw new IllegalArgumentException("Malformed markup encountered.");

        int startIndex = -1;
        int endIndex = -1;
        String tag = null;

        // Parse the tag, ignoring angle brackets and stopping at the first space if there is one.
        for (int i = 0; i < html.length(); i++) {
            char c = html.charAt(i);

            if (c == '<') {
                startIndex = i + 1;
            } else if (c == ' ' || c == '>') {
                endIndex = i;
                break;
            }
        }

        // Sanity check; make sure that we're not returning a 0-length string or attempting to
        // access characters not in the string.
        if (startIndex < endIndex && endIndex < html.length()) {
            tag = html.substring(startIndex, endIndex);
        }

        return tag;
    }
}