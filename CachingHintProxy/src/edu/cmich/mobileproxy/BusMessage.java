/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy;

/**
 *
 * @author pseeling
 */
public class BusMessage {

    private String action;
    private String receiver;
    private Object body;

    public String getAction() {
        return action;
    }

    public void setAction(String sender) {
        this.action = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    
}
