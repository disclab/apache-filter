/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy;

import java.io.Serializable;

/**
 * @author vmuser
 */
public class WebObject implements Serializable {
    private String TAG = "WebObject";
    private String MIME_TYPE ="";
    private long expiration;
    private String contentHash = "";

    public WebObject() {
    }

    /**
     * @return the MIME_TYPE
     */
    public String getMIME_TYPE() {
        return MIME_TYPE;
    }

    /**
     * @param MIME_TYPE the MIME_TYPE to set
     */
    public void setMIME_TYPE(String MIME_TYPE) {
        this.MIME_TYPE = MIME_TYPE;
    }

    /**
     * @return the expiration
     */
    public long getExpiration() {
        return expiration;
    }

    /**
     * @param expiration the expiration to set
     */
    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    /**
     * Returns the hexadecimal string representing the object's content.
     * @return The content hash value string.
     */
    public String getContentHash() { return contentHash; }

    /**
     * Sets the hexadecimal string that represents the object's content.
     * @param contentHash The new hash string value.
     */
    public void setContentHash(String contentHash) { this.contentHash = contentHash; }
}
