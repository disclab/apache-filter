/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cmich.mobileproxy;

import static edu.cmich.mobileproxy.LogUtils.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.cmich.mobileproxy.cacheman.DeleteFileTask;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_LENGTH;
import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import java.util.prefs.Preferences;

/**
 * @author pseeling
 */
public class ObjectMap {

    private static String TAG = "ObjectMap";

    public static ConcurrentHashMap<String, WebObject> map = new ConcurrentHashMap<String, WebObject>();
    public static ConcurrentHashMap<String, Long> savingsMapBytes = new ConcurrentHashMap<String, Long>();
    public static ConcurrentHashMap<String, Long> savingsMapRequests = new ConcurrentHashMap<String, Long>();

    public static File cacheFilePath;

    public static long totalRequestsSaved = 0;
    public static long totalBytesSaved = 0;
    public static long totalRequestsMade = 0;

    private static Preferences preferences = Preferences.userRoot().node("CacheConnect");

    public static void loadObjectMap() {

        String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes()));
        File file = new File(storagePath, "maps.ser");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (file.exists()) {
            //1002 size because file maybe be empty, but still have size of 1002 due to meta data possibly (maps file that has been filled previously and then purged)
            if (file.length() > 1002) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    map = (ConcurrentHashMap) ois.readObject();
                    ois.close();
                    //return true;
                    //return;
                } catch (FileNotFoundException ex) {
                    LOGE(TAG, "File not found");
                    Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    LOGE(TAG, "IOException " + ex.getLocalizedMessage());
                    Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    LOGE(TAG, "ClassNotFoundException " + ex.getLocalizedMessage());
                    Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                LOGD(TAG, "maps.ser file size is 0, not attempting read.");
            }
        }
    }

    public static boolean saveObjectMap() {
        //LOGD(TAG, "Saving objectMap");

        String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes()));
            File storage = new File(storagePath);
            File file = new File(storage.getAbsolutePath(), "maps.ser");

            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (Exception e) {
                    LOGD(TAG, "Error creating maps file");
                }
            }

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(file, false);
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(map);
                oos.close();
                return true;
            } catch (FileNotFoundException ex) {
                LOGE(TAG, "File not found");
                Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                LOGE(TAG, "IOException " + ex.getLocalizedMessage());
                ex.printStackTrace();
                Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return false;
    }

    public static void writeResponseToFile(HttpObject httpObject, ByteArrayOutputStream response, String responseName,
            String sha1) {

        //Typical format of date string: Sun, 13 Sep 2015 22:50:01 GMT
        DateFormat expiresFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

        long receiveTime = System.currentTimeMillis();
        boolean expirationWasSet = false;

        // Add to map
        WebObject wO = new WebObject();
        List<Map.Entry<String, String>> hentries = ((HttpResponse) httpObject).headers().entries();
        for (Map.Entry<String, String> element : hentries) {
            if (element.getKey().equalsIgnoreCase("Content-Type")) {
                wO.setMIME_TYPE(element.getValue());
            }
            // Examples: Cache-Control: max-age=3600, must-revalidate Expires: Fri, 30 Oct 1998 14:19:41 GMT
            if (element.getKey().equalsIgnoreCase("Cache-Control")) {

                String[] values = element.getValue().toLowerCase().replaceAll("\\s", "").split(",");
                for (String value : values) {
                    if (value.contains("max-age")) {

                        try {
                            long expirationTime = Long.parseLong(value.split("=")[1]);
                            wO.setExpiration(receiveTime + expirationTime * 1000);
                            expirationWasSet = true;
                        } catch (Exception e) {
                            //TODO: Insert a default timestamp if time parsing fails??
                            LOGE(TAG, "Error converting max-age " + element.getValue());
                        }
                        break;
                    }

                }
            }
            if ((element.getKey().equalsIgnoreCase("expires"))) { // alternative,
                try {
                    //Typical format of date string: Sun, 13 Sep 2015 22:50:01 GMT
                    Date date = expiresFormatter.parse(element.getValue());

                    // TODO: cleanup, was this static?
                    wO.setExpiration(date.getTime());
                    expirationWasSet = true;
                } catch (Exception e) {
                    //TODO: Insert a default timestamp if time parsing fails??
                    LOGE(TAG, "Error formatting date: " + element.getValue());
                }
            }
        }
        ObjectMap.map.put(sha1, wO);

        if (!fileExistsInCache(sha1)) {
            try {
                OutputStream outputStream = new FileOutputStream(
                        new File(cacheFilePath.getAbsolutePath() + "/" + sha1));
                try {
                    response.writeTo(outputStream);
                    outputStream.close();
                } catch (IOException ex) {
                    LOGE(TAG, "Error writing file to cache.");
                    LOGE(TAG, ex.toString());
                }
            } catch (FileNotFoundException ex) {
                LOGE(TAG, "Unable to write file to cache location. Sha1: " + sha1);
            }
        } else {
            LOGD(TAG, "File already exists in cache location. Response Name: " + responseName + " sha1: " + sha1);
        }
    }

    public static boolean fileExistsInCache(String sha1) {
        File myFile;
        return (myFile = new File(cacheFilePath.getAbsolutePath() + "/" + sha1)).exists();
    }

    public static boolean deleteFileFromMap(String sha1) {
        try {
            map.remove(sha1);
        } catch (Exception e) {
            LOGE(TAG, "Unable to remove " + sha1 + " from map");
            return false;
        }
        return deleteFileFromCache(sha1);
    }

    public static boolean deleteFileFromCache(String sha1) {
        File myFile = new File(cacheFilePath.getAbsolutePath() + "/" + sha1);
        new DeleteFileTask(myFile).start();
        return true;
    }

    public static byte[] getFileFromCache(String fileName) {
        byte[] filedata = null;
        try {
            filedata = org.apache.commons.io.FileUtils
                    .readFileToByteArray(new File(cacheFilePath.getAbsolutePath() + "/" + fileName));
        } catch (IOException ex) {
            LOGE(TAG, ex.toString());
        }
        return filedata;
    }

    public static FullHttpResponse getFullHttpResponseFromCache(String filename) {
        FullHttpResponse FHR = new DefaultFullHttpResponse(HttpVersion.HTTP_1_0, HttpResponseStatus.OK,
                Unpooled.wrappedBuffer(getFileFromCache(filename)));

        FHR.headers().set(CONTENT_LENGTH, FHR.content().readableBytes());
        WebObject wO;
        if ((wO = ObjectMap.map.get(filename)) != null) {
            FHR.headers().set(CONTENT_TYPE, wO.getMIME_TYPE());
            // TODO: add the other relevant header fields here as well?
            LOGE(TAG, ObjectMap.map.get(filename).toString());
        }

        return FHR;
    }

    public static void initCacheFilePath() {
        String storagePath = new String(preferences.getByteArray(Constants.STORAGE_KEY, Desktop_Proxy.DEFAULT_STORAGE_LOCATION.getBytes()));
        File storage = new File(storagePath);
        storage.mkdir();
        cacheFilePath = new File(storage.getAbsolutePath(), Constants.CACHE_NAME);
        cacheFilePath.mkdir();
        LOGD(TAG, "New Cache File Path: " + cacheFilePath);
    }

    public static void incrementTotalRequestsMade() {
        totalRequestsMade++;

        //TODO: Update shared prefs here or on destroy()?
        preferences.putLong(Constants.REQUESTS_MADE_TOTAL_KEY, totalRequestsMade);
    }

    public static void accumulateSavings(int bytesSaved) {
        LOGD(TAG, "Accumulating savings");
        LOGD(TAG, "bytesSaved: " + bytesSaved);
        totalBytesSaved += bytesSaved;
        totalRequestsSaved += 1;

        //String date = Constants.DATE_FORMATTER.format(Calendar.getInstance().getTime());
        //Append string value onto end of key so that shared preferences since date is the key value, otherwise
        //requests and bytes will each have values with the same date key value and overwrite each other.
        //String requestsKey = date + Constants.MAP_KEY_REQUESTS;
        //String bytesKey = date + Constants.MAP_KEY_BYTES;
        //Update total requests savings for the day
        savingsMapRequests.put(Constants.REQUESTS_SAVED_KEY, totalRequestsSaved);
        savingsMapBytes.put(Constants.BYTES_SAVED_KEY, totalBytesSaved);

        LOGD(TAG, "bytesSaved total: " + totalBytesSaved);

        //TODO: Change so that shared preferences aren't updated everytime? More efficient to wait until activity is destroyed or something else
        saveSavings();
    }

    public static void extendExpiry(String cacheItem, String contentHash) {
        WebObject o = map.getOrDefault(cacheItem, null);

        if (o != null) {
            o.setExpiration(System.currentTimeMillis() + 5000);  // Add 5 seconds.
            System.out.println("Expires: " + o.getExpiration());
        }
    }

    //Save contents of hashmaps holdings savings in terms of bytes and requestss to disk
    private static void saveSavings() {

        //Save contents of requests saved to shared prefs
        for (String s : savingsMapRequests.keySet()) {
            preferences.putLong(s, savingsMapRequests.get(s));
        }

        //Save contents of bytes saved to shared prefs
        for (String s : savingsMapBytes.keySet()) {
            preferences.putLong(s, savingsMapBytes.get(s));
        }

    }
}
