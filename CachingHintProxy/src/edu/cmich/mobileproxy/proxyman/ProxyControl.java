package edu.cmich.mobileproxy.proxyman;

import edu.cmich.mobileproxy.caching_hint.HashedResourceHtmlTag;
import edu.cmich.mobileproxy.caching_hint.HtmlFilter;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

import java.io.ByteArrayOutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

import edu.cmich.mobileproxy.Constants;
import edu.cmich.mobileproxy.ObjectMap;
import edu.cmich.mobileproxy.cacheman.CacheService;
import edu.cmich.mobileproxy.cacheman.CacheServiceIncomingHandler;
import edu.cmich.mobileproxy.cacheman.WriteResponseToFileTask;
import edu.cmich.mobileproxy.pssha1;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultHttpContent;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpContent;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.LastHttpContent;

import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;

/**
 * An IntentService subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class ProxyControl extends Thread {

    private final String TAG = "ProxyControl";
    private static boolean isRunning = false;
    // FROM CACHE
    private long requestTime;
    private ProxyControlIncomingHandler incomingHandler;

    @Override
    public void run() {
        LOGD(TAG, "Service Started.");
        //showNotification();
        //timer.scheduleAtFixedRate(new TimerTask(){ public void run() {onTimerTick();}}, 0, 100L);
        //incomingHandler = new ProxyControlIncomingHandler(this);
        //mMessenger = new Messenger(incomingHandler);
        isRunning = true;

        // TODO: convert port and check if available, return error otherwise schedule reconnection

        HttpFiltersSourceAdapter HFSA = new HttpFiltersSourceAdapter() {
            public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) {
                return new HttpFiltersAdapter(originalRequest) {
                    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                    int contentLength = -1;

                    @Override
                    public HttpResponse clientToProxyRequest(HttpObject httpObject) {
                        //System.out.println((char)27 + "[42mclient --> proxy     server" + (char) 27 + "[0m : " + originalRequest.getUri());
                        requestTime = System.currentTimeMillis();
//                        try {
                            String sha1 = pssha1.SHAsum(originalRequest.getUri().getBytes());

                            //Increment total requests made
                            ObjectMap.incrementTotalRequestsMade();

                            if (ObjectMap.map.containsKey(sha1)) {
                                if (ObjectMap.fileExistsInCache(sha1)) {
                                    LOGD(TAG, "File found!");

                                    if (ObjectMap.map.get(sha1).getExpiration() > requestTime) {
                                        FullHttpResponse FHR = ObjectMap.getFullHttpResponseFromCache(sha1);
                                        //Accumulate savings since item was found
                                        ObjectMap.accumulateSavings(FHR.content().readableBytes());
                                        return FHR;
                                    } else {
                                        LOGD(TAG, "Client to Proxy Request: Deleting outdated file!");
                                        ObjectMap.map.remove(sha1);
                                        if (!ObjectMap.deleteFileFromCache(sha1)) {
                                            LOGE(TAG, "Client to Proxy Request: Error deleting file from cache: " + sha1);
                                        }
                                        return null;
                                    }
                                } else {
                                    LOGD(TAG, "removing sha1: " + sha1);
                                    ObjectMap.map.remove(sha1);
                                }
                            }
//                        } catch (NoSuchAlgorithmException ex) {
//                            System.err.println(ex.toString());
//                        }
                        return null;
                    }

                    @Override
                    public HttpResponse proxyToServerRequest(HttpObject httpObject) {
                        //System.out.println((char)27 + "[42mclient     proxy --> server" + (char) 27 + "[0m : " + originalRequest.getUri());
                        return null;
                    }

                    @Override
                    public HttpObject serverToProxyResponse(HttpObject httpObject) {
                        //System.out.println((char)27 + "[42mclient     proxy <-- server" + (char) 27 + "[0m : " + originalRequest.getUri());

//                        try {
                            if (httpObject instanceof HttpResponse) {
                                FullHttpResponse hr = (FullHttpResponse) httpObject;
                                List<Map.Entry<String, String>> hentries = hr.headers().entries();
                                String contentType = null;

                                for (Map.Entry<String, String> element : hentries) {
                                    if (element.getKey().equalsIgnoreCase("Content-Length")
                                            && contentLength == -1) {
                                        contentLength = Integer.parseInt(element.getValue());
                                    } else if (element.getKey().equalsIgnoreCase("Content-Type")) {
                                        contentType = element.getValue();
                                    }
                                }

                                // If the item is an HTML page, parse it and look for caching hints.  The
                                // page should include the custom attribute with a corresponding value
                                // containing the URL hash and content hash.
                                if (contentType != null && contentType.equals("text/html")) {
                                    byte[] buf = new byte[hr.content().capacity()];
                                    hr.content().getBytes(0, buf);
                                    // Get the tags containing potentially cached resources.
                                    String content = new String(buf);

                                    try {
                                        HashedResourceHtmlTag[] tags = HtmlFilter
                                                .extractResourceTags(content, "data-resource-sha1");
                                        tags = HtmlFilter.standardizeSourceLinks(tags, originalRequest.getUri());

                                        for (HashedResourceHtmlTag tag : tags) {
                                            ObjectMap.extendExpiry(tag.uriHash, tag.contentHash);
                                        }
                                    } catch (Exception ex) {
                                        // Do nothing; proxy will download again.
                                    }
                                }

                            } else if (httpObject instanceof DefaultHttpContent) {
                                DefaultHttpContent dhc = (DefaultHttpContent) httpObject;
                                byte[] buf = new byte[dhc.content().capacity()];
                                dhc.content().getBytes(0, buf);
                                byteOut.write(buf, 0, buf.length);

                                if (byteOut.size() == contentLength) {
                                    String sha1 = pssha1.SHAsum(originalRequest.getUri().getBytes());
                                    //ObjectMap.writeResponseToFile(dhc, byteOut, originalRequest.getUri(), sha1);
                                    //Changed above code to execute in form of async task
                                    new WriteResponseToFileTask(dhc, byteOut, originalRequest.getUri(), sha1, ObjectMap.cacheFilePath).start();
                                    byteOut.reset();
                                    contentLength = -1;
                                }

                            } else if (httpObject instanceof HttpContent) {
                                // Handled in proxyToClientResponse()
                                System.out.println("Found HttpContent");

                            } else if (httpObject instanceof LastHttpContent) {
                                LastHttpContent lhc = (LastHttpContent) httpObject;
                                byte[] buf = new byte[lhc.content().capacity()];
                                List<Map.Entry<String, String>> hentries = lhc.trailingHeaders().entries();
                                lhc.content().getBytes(0, buf);
                                byteOut.write(buf, 0, buf.length);
                                String sha1 = pssha1.SHAsum(originalRequest.getUri().getBytes());
                                //ObjectMap.writeResponseToFile(lhc, byteOut, originalRequest.getUri(), sha1);
                                //Changed above code to execute in form of async task
                                new WriteResponseToFileTask(lhc, byteOut, originalRequest.getUri(), sha1, ObjectMap.cacheFilePath).start();
                                byteOut.reset();
                            } else {
                                LOGE(TAG, "httpObject not instance any");
                            }
//                        } catch (NoSuchAlgorithmException ex) {
//                            LOGE(TAG, "NoSuchAlgorithmException ex");
//                        }
                        return httpObject;
                    }

                    @Override
                    public HttpObject proxyToClientResponse(HttpObject httpObject) {
                        //System.out.println((char)27 + "[42mclient <-- proxy     server" + (char) 27 + "[0m : " + originalRequest.getUri());

                        if (httpObject instanceof HttpContent) {
                            FullHttpResponse lhc = (FullHttpResponse) httpObject;
                            int s = lhc.content().readableBytes();
                            byte[] buf = new byte[lhc.content().capacity()];
                            lhc.content().getBytes(0, buf);
                            byteOut.write(buf, 0, buf.length);

                            try {
                                String sha1 = pssha1.SHAsum(originalRequest.getUri().getBytes("UTF-8"));
                                System.out.println("Writing " + originalRequest.getUri());
                                //ObjectMap.writeResponseToFile(lhc, byteOut, originalRequest.getUri(), sha1);
                                //Changed above code to execute in form of async task
                                new WriteResponseToFileTask(lhc, byteOut, originalRequest.getUri(), sha1, ObjectMap.cacheFilePath).start();
                            } catch (Exception e) {
                                LOGE(TAG, e.toString());
                                LOGE(TAG, originalRequest.getUri());
                            }
                        }
                        return httpObject;
                    }
                };
            }
        };

        HttpProxyServer server = DefaultHttpProxyServer.bootstrap().withPort(Constants.PROXY_SERVER_PORT).withFiltersSource(HFSA).start();

    }
}
