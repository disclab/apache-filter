package edu.cmich.mobileproxy.proxyman;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import edu.cmich.mobileproxy.CacheServiceMessage;
import edu.cmich.mobileproxy.Desktop_Proxy;
import edu.cmich.mobileproxy.cacheman.CacheServiceIncomingHandler;

import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGI;

/**
 * Created by johns4ta on 9/9/2015.
 */
public class ProxyControlIncomingHandler {

    private Bus bus;

    private String TAG = "ProxyMessageHandler";

    ProxyControlIncomingHandler() {
        // Register with the event bus
        bus = Desktop_Proxy.getBusInstance();
        bus.register(this);
    }

    @Subscribe
    public void handleMessage(CacheServiceMessage msg) {
        if (msg.getDestination().equalsIgnoreCase(TAG)) {

            LOGD(TAG, "handleMessage Reached. msg.action contents: " + msg.getAction());

            switch (msg.getAction()) {
                case CacheServiceIncomingHandler.MSG_CHECK_SHA:
                    //mClients.add(msg.replyTo);
                    break;
                case CacheServiceIncomingHandler.MSG_INSERT_FILE:
                    //mClients.remove(msg.replyTo);
                    break;
                case CacheServiceIncomingHandler.MSG_DELETE_FILE:
                    //incrementby = msg.arg1;
                    LOGI(TAG, "HELLO! " + msg.getMessage());
                    break;
                case CacheServiceIncomingHandler.MSG_SET_INT_VALUE:
                    LOGD(TAG, "MSG_SET_INT_VALUE " + Integer.parseInt(msg.getMessage()));
                    //mClients.remove(msg.replyTo);
                    break;
                case CacheServiceIncomingHandler.MSG_CHECK_SHA_FILE:
                    boolean fileExists = (Boolean) msg.getBody();
                    if (fileExists) {
                        LOGD(TAG, "File exists in cache");
                    } else {
                        LOGD(TAG, "File does NOT in cache");
                    }
                    break;
            }
        }
    }

}
