package edu.cmich.mobileproxy.proxyman;

import java.io.IOException;
import java.net.Socket;

import static edu.cmich.mobileproxy.LogUtils.LOGD;

/**
 * Created by johns4ta on 9/24/2015.
 */
//Checks whether or not the local port passed in is in use or not.
public class CheckIfPortOpenTask {

    private static String TAG = "CheckIfPortOpenTask";
    private int port;

    public static boolean checkPort(int port) {

        String host = "127.0.0.1";
        boolean result = false;

        try {
            try {
                (new Socket(host, port)).close();
                result = true;
            } catch (IOException e) {
                LOGD(TAG, "Error checking if port in use");
                e.printStackTrace();
            }

        } catch (Exception e) {
            LOGD(TAG, "Error checking if port in use");
            e.printStackTrace();
        }
        return result;
    }
}
