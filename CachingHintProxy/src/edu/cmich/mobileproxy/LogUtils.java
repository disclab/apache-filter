package edu.cmich.mobileproxy;

import java.lang.management.ManagementFactory;
import java.util.regex.Pattern;

/**
 * Created by pseeling on 9/3/15.
 */
public class LogUtils {

    private final static Pattern debugPattern = Pattern.compile("-Xdebug|jdwp");

    public static void LOGD(String tag, String message) {
        if (isDebugging()) {
            System.out.println(tag + ": "+ message);
        }
    }

    public static void LOGV(String tag, String message) {
        if (isDebugging()) {
            System.out.println(tag + ": "+ message);
        }
    }

    public static void LOGI(String tag, String message) {
        if (isDebugging()) {
            System.out.println(tag + ": "+ message);
        }
    }

    public static void LOGW(String tag, String message) {
        if (isDebugging()) {
            System.out.println(tag + ": "+ message);
        }
    }

    public static void LOGE(String tag, String message) {
        if (isDebugging()) {
            System.err.println(tag + ": "+ message);
        }
    }

    public static boolean isDebugging() {

        return true;
        /*
        for (String arg : ManagementFactory.getRuntimeMXBean().getInputArguments()) {
            if (debugPattern.matcher(arg).find()) {
                return true;
            }
        }

        return false;
*/
    }

}
