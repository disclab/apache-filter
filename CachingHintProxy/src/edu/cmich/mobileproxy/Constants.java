package edu.cmich.mobileproxy;

import com.squareup.otto.Bus;
import java.io.File;

/**
 * Created by johns4ta on 10/30/2015.
 */
public interface Constants {

    //Keys for accessing shared preference values
    //Key for accessing hour from shared preferences that sync alarm to set of occur at
    public static String SYNC_TIME_HOUR_KEY = "SYNC_TIME_HOUR_KEY";

    //Key for accessing minute  from shared preferences that sync alarm to set of occur at
    public static String SYNC_TIME_MINUTE_KEY = "SYNC_TIME_MINUTE_KEY";

    //Key for accessing IP addresses from shared preferences
    public static String ADDRESSES_KEY = "ADDRESSES_KEY";

    //Key for accessing total amount of requests saved
    final static String REQUESTS_SAVED_KEY = "REQUESTS_SAVED";

    //Key for accessing total amount of bytes saved
    final static String BYTES_SAVED_KEY = "BYTES_SAVED";

    //Key for accessing total number of requests made
    final static String REQUESTS_MADE_TOTAL_KEY = "REQUESTS_MADE_TOTAL_KEY";

    //Key for accessing cache storage location on device
    public static final String STORAGE_KEY = "STORAGE_LOCATION";

    //Key for accessing ip address of desktop computer that device will sync with
    public static final String DESKTOP_IP_KEY = "DESKTOP_IP_KEY";

    //Name of folder that holds responses
    public static final String CACHE_NAME = "CacheFiles";

    //Holds decision on whether or not user has selected that devices be required to be plugged in to sync
    public static final String REQUIRE_PLUGGED_IN_TO_SYNC = "REQUIRE_PLUGGED_IN_TO_SYNC";

    //Bluetooth Constants
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int BLUETOOTH_CONNECTION_ERROR = 6;

    //Device is letting other device know it's IP address
    public static final String SEND_IP = "SEND_IP";

    //Device is letting other device know it is going to send its cache contents to it
    public static final String SEND_CACHE_CONTENTS = "SEND_CACHE_CONTENTS";

    //Device is letting other device know it is NOT plugged into a power source
    public static final String DEVICE_NOT_PLUGGED_IN = "DEVICE_NOT_PLUGGED_IN";

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "DEVICE_NAME";
    public static final String TOAST = "TOAST";

    //DecisionMakerMan Constants
    public static final String REQUEST_SER_FILE = "REQUEST_SER_FILE";

    //Pwo contains requested SER file
    public static final String SEND_SER_FILE = "SEND_SER_FILE";

    //List of items needed by phone has been sent
    public static final String SEND_LIST_NEEDED_ITEMS = "SEND_LIST_NEEDED_ITEMS";

    //Pwo contains a response that needs to be stored on receiving device
    public static final String SEND_INDIVIDUAL_RESPONSE = "SEND_INDIVIDUAL_RESPONSE";

    //Last item from requested item has been sent
    public static final String LAST_SEND_INDIVIDUAL_RESPONSE = "LAST_SEND_INDIVIDUAL_RESPONSE";

    public static final String REQUEST_PUSH_RESPONSE = "REQUEST_PUSH_RESPONSE";

    //Port number used for communicating over the network
    public static int SERVER_PORT = 5000;

    //Port proxy server is running on
    public static int PROXY_SERVER_PORT = 8080;

    //Formatter used for accessing and retreiving values from hashmaps where the key is the date
    // public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd-MMM-yyyy");
    //public static final String MAP_KEY_REQUESTS = "-REQUESTS";
    //public static final String MAP_KEY_BYTES = "-BYTES";
    //Time devices attempt to connect over bluetooth before timing out
    public static final int BLUETOOTH_TIMEOUT = 60000;

}
