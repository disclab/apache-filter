package edu.cmich.mobileproxy;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import edu.cmich.mobileproxy.cacheman.DeleteFileTask;

import static edu.cmich.mobileproxy.LogUtils.LOGD;
import static edu.cmich.mobileproxy.LogUtils.LOGE;
import java.nio.ByteBuffer;

/**
 * Created by johns4ta on 10/16/2015.
 */
public class Utils {

    private static String TAG = "Utils";
    
    public static PassableWebObject convertByteBufferToPWO(ByteBuffer byteBuffer) {
        LOGD(TAG, "Converting byte array to pwo");
        ByteArrayInputStream in = new ByteArrayInputStream(byteBuffer.array());
        ObjectInputStream is = null;
        PassableWebObject pwo = new PassableWebObject();

        try {
            is = new ObjectInputStream(in);
        } catch (IOException e) {
            LOGD(TAG, "Error reading from stream");
            e.printStackTrace();
        }
        try {
            pwo = (PassableWebObject) is.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            is.close();
            in.close();
        } catch (IOException e) {
            LOGD(TAG, "Error closing streams!");
            e.printStackTrace();
        }
        return pwo;
    }
    
/*
    public static PassableWebObject convertByteBufferListToPWO(ByteBufferList byteBufferList) {
        LOGD(TAG, "Converting byte array to pwo");
        ByteArrayInputStream in = new ByteArrayInputStream(byteBufferList.getAllByteArray());
        ObjectInputStream is = null;
        PassableWebObject pwo = new PassableWebObject();

        try {
            is = new ObjectInputStream(in);
        } catch (IOException e) {
            LOGD(TAG, "Error reading from stream");
            e.printStackTrace();
        }
        try {
            pwo = (PassableWebObject) is.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            is.close();
            in.close();
        } catch (IOException e) {
            LOGD(TAG, "Error closing streams!");
            e.printStackTrace();
        }
        return pwo;
    }
*/
    
    public static byte[] convertPWOToByteArray(PassableWebObject pwo) {
        LOGD(TAG, "Converting PWO to byte array");
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = null;
        try {
            LOGD(TAG, "Attempting to write to output stream");
            os = new ObjectOutputStream(out);
            os.writeObject(pwo);
        } catch (IOException e) {
            LOGD(TAG, "Error writing to output streams");
            e.printStackTrace();
        }

        try {
            out.flush();
            out.close();
            os.flush();
            os.close();
        } catch (Exception e) {
            LOGD(TAG, "Error flushing/closing output streams");
            e.printStackTrace();
        }
        return out.toByteArray();
    }

    //Write contents of requestsContents byteArray to file passed in
    public static boolean writesByteArrayToDisk(File file, byte[] requestContents) {
        try {
            file.createNewFile();
            LOGD(TAG, "Attempting to write to disk: " + file.getAbsolutePath());
            FileUtils.writeByteArrayToFile(file, requestContents);
        } catch (FileNotFoundException ex) {
            LOGE(TAG, "File not found");
            Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            LOGE(TAG, "IOException " + ex.getLocalizedMessage());
            Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    //to get needed items and return needed items.
    public static ArrayList<String> determineNeededCacheItems(String incomingSerFilePath) {
        ConcurrentHashMap<String, WebObject> incomingMap = new ConcurrentHashMap<String, WebObject>();

        ObjectMap.saveObjectMap();
        ObjectMap.loadObjectMap();         //Need to load map into memory from file

        File incomingSerFile = new File(incomingSerFilePath);

        //1002 size because file maybe be empty, but still have size of 1002 due to meta data possibly (maps file that has been filled previously and then purged)
        if (incomingSerFile.length() > 1002) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(incomingSerFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                incomingMap = (ConcurrentHashMap) ois.readObject();
                ois.close();
            } catch (FileNotFoundException ex) {
                LOGE(TAG, "File not found");
                Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                LOGE(TAG, "IOException " + ex.getLocalizedMessage());
                Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                LOGE(TAG, "ClassNotFoundException " + ex.getLocalizedMessage());
                Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(ObjectMap.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            LOGD(TAG, "Incoming ser file size is 0, not attempting read.");
        }

        ArrayList<String> neededItems = new ArrayList<String>();

        Iterator it = incomingMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String sha = pair.getKey().toString();
            WebObject wo = (WebObject) pair.getValue();

            if (!ObjectMap.map.containsKey(sha)) {
                neededItems.add(sha);
            }
        }
        LOGD(TAG, "List of needed items");
        LOGD(TAG, neededItems.toString());

        //Delete file file that was read from
        new DeleteFileTask(new File(incomingSerFilePath)).start();

        return neededItems;
    }

    // TODO: Delete, in for testing purposes, outputs contents of cache
    public void readCacheContents() {
        ObjectMap.saveObjectMap();

        Iterator it = ObjectMap.map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String sha = pair.getKey().toString();
            WebObject wo = (WebObject) pair.getValue();
            if (wo.getMIME_TYPE() != null) {
                LOGD(TAG, wo.getMIME_TYPE());
            }

            LOGD(TAG, Long.toString(wo.getExpiration()));
        }
    }

    //Check if a given ip address is valid
    public static boolean isValidIP(String ip) {
        Pattern VALID_IPV4_PATTERN = null;
        Pattern VALID_IPV6_PATTERN = null;
        final String ipv4Pattern = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])";
        final String ipv6Pattern = "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}";

        try {
            VALID_IPV4_PATTERN = Pattern.compile(ipv4Pattern, Pattern.CASE_INSENSITIVE);
            VALID_IPV6_PATTERN = Pattern.compile(ipv6Pattern, Pattern.CASE_INSENSITIVE);
        } catch (PatternSyntaxException e) {
            LOGD(TAG, "Unable to compile pattern to check ip address");
        }

        Matcher m1 = VALID_IPV4_PATTERN.matcher(ip);
        Matcher m2 = VALID_IPV6_PATTERN.matcher(ip);
        if (m1.matches() || m2.matches()) {
            return true;
        }
        return false;
    }
}
