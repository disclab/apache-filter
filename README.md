# Forward Caching Hint Demo

This repository contains three separate projects that work together to demonstrate reduced network traffic and improved web browsing via forward caching hints.

## Background

The purpose of this demo is to show savings in network bandwidth by reducing the number of resource downloads by the client.  Client machines use caching to avoid redundant downloads of resources, but the caching system relies on an expiration date to determine whether or not to download the resource again.  The main limitation is that the webserver cannot know when a resource might change, so it must set an expiration on each resource requested, which forces the client to download the resource again, whether or not the resource has changed.  One workaround, of course, is for the client to set the `If-Modified-Since` header, but that requires one round-trip communication (request/response) with the server solely to determine whether the resource has changed.

We can eliminate the inefficiency of an extra request/response if the server sends a hint along with the originally-requested HTML document.  This demo provides a hint in the form of a SHA-1 hash of each resource that is referenced by the requested HTML page.  The idea behind the demo is to intercept the original HTML request, inspect the document for any resources it may reference, calculate the SHA-1 hash of each resource, and inject those hashes into the response before it is returned.  Upon receipt of the document, the client may then parse the hashes and use them to determine whether the content has changed before potentially unnecessarily requesting them from the server.

## Components

There are two main components that work with the caching hints:

* **[CachingHintProxy](#proxy)** - A client-side proxy through which websites are requested.  This proxy maintains the cache for the client and only requests resources which have expired.  The proxy also extracts the forward caching hints and uses them to determine whether to extend the expiration time.
* **[Scripts](#filter)** - Python scripts used on the webserver to intercept the original request and inject the caching hints into the response.

In addition to the client and server side portions, there is a demo website (located in the [Website](#site) directory) which contains resources that will be requested by the client but not before being hashed by the server.

### CachingHintProxy<a name="proxy"></a>

The CachingHintProxy project is a modified version of the [Desktop_Proxy](http://cps-pss-git.cps.cmich.edu/pseeling/Desktop_Proxy) project, which uses the [LittleProxy_HTTPS_Base](http://cps-pss-git.cps.cmich.edu/pseeling/LittleProxy-HTTPS-Base) with asynchronous caching tasks.  CachingHintProxy includes functionality for intercepting the webserver response, extracting the forward caching hints, and updating object expirations as necessary.

##### Workflow

The workflow for the CachingHintProxy is nearly identical to that of the Desktop_Proxy (modifiations denoted by *italics*):

1. The client requests a resource via the proxy.
2. The proxy, which uses a SHA-1 hash of the resource URL, checks its cache index to see if the requested resource is already cached.
   * If the resource is cached and not expired, it generates a response to the client.
   * If the resource is cached and expired, it deletes the cached copy and continues.
3. The proxy forwards the request to the webserver.
4. The webserver responds to the proxy, *which attempts the caching hint extraction (see below).*
5. The proxy then adds the resource to the cache:
   * Generate a SHA-1 hash of the resource URL.
   * Add an entry to the index, using the hash as the key.
   * Set the expiration if the response includes the `Expires` header.
   * Save a copy of the response payload, using the hash as the file name.
6. The proxy forwards the response to the client.

##### Caching Hint Extraction

After the proxy receives the response from the webserver, but before it caches the resource or forwards the response to the client, it will extract the caching hints and update the cache index as necessary.  First, the proxy checks that the resource has MIME type `text/html`.  If it doesn't, then the process will continue as outlined above.  Otherwise, the proxy will extract the caching hints:
1. Parse the document and extract all HTML tags that link to other resources.
   * These include `script`, `link`, `img`, `iframe`, `embed`, `object`, `video`, `audio`, `source`, `track`, and `area`.
2. Extract the caching hint from the tags.
   * The caching hint is stored as an HTML5 attributed called `data-resource-sha1`.  Its value contains a SHA-1 hash of the resource's URL and a SHA-1 hash of the resource itself, separated by a colon.  Both are provided to avoid having to hash the resource URL multiple times on the client side.
3. Check the cache index for the resource (specified by hashed URL):
   * If it exists, it is expired, and the content hash indicates the resource is unchanged, set the expiration to 5 seconds from the current time (to allow time for the client to fetch the remaining resources).
   * If it does not exist, is not expired, or if the content has changed, the cache manager will delete the resource and the new version will be downloaded.

##### Content Hash

One last point of note is that the forward caching hint process does require that the cache manager keep track of the resource content hash, which requires an extra attribute on the `WebObject` class.  This stored value is compared with the hash sent by the server to determine whether the resource has changed.

### Scripts<a name="filter"></a>
The Scripts project contains a Python script designed to work on an Apache server.  This script intercepts the HTTP response before it is returned from the server and injects the forward caching hints in the HTML tags.

##### Apache `mod_filter` Module
The Apache server's `mod_filter` [module](https://httpd.apache.org/docs/2.4/mod/mod_filter.html) must be enabled and the site configured to use the Python script.  The `mod_filter` module is enabled by running:

```
a2enmod mod_filter
```

and the site configured to run the script, first by defining the filter:

```
ExtFilterDefine inject-hash mode=output \
    intype=text/html outtype=text/html \
    cmd="/usr/bin/python /path/to/script.py"
```

then by configuring which script will be run:

```
<LocationMatch "/*">
    SetOutputFilter inject-hash
    ExtFilterOptions LogStderr
</LocationMatch>
```

The `ExtFilterDefine` directive defines the script's name and location for Apache, and sets the server to run the script only when the request has MIME type `text/html`.  It also tells Apache that the output will have the same MIME type as  the input.  Additionally, the `LocationMatch` directive tells Apache to run the script on every HTML page requested.  This particular configuration also logs any errors that occur.

##### Request/Response Workflow
The workflow for an HTML page request is fairly simple:
1. Server receives the HTTP request.
2. Apache builds builds the response either by loading a static HTML page or by running a script (such as PHP, Python, Perl, etc.) to create a dynamic page.
3. Apache filters the response through the script, which uses `stdin` and `stdout` to process the request.
4. The modified response is returned to the client.

##### Hint Injection Workflow
The injection process is as follows:
1.  Read the response data from `stdin` and store in a string variable.
2.  Parse the data character-by-character to extract the HTML tags.  A tag is considered the text between and including pairs of angle brackets (&lt; and &gt;).
3.  Once a tag is found, check whether it can reference an external resource.  If not, ignore it.
    * `script`, `link`, `img`, `iframe`, `embed`, `object`, `video`, `audio`, `source`, `track`, and `area` tags call all contain external resources.
4.  If the tag can reference an external resource, check whether it actually does.
    * Tags with a `src` or `href` attribute and value reference external resources.  If neither are present, then the content is encoded within the page itself.  *Note, however, that the script does not currently allow for the content to be embedded as a base64-encoded string.*
5.  Extract the reference from the tag and convert it to a file path.
   	* The reference is a URL that may reference a resource on the local server or a remote server.  It also may be absolute or relative, and may be relative to the server root URL or relative to the page, so it must first be converted to a path.
6.  Get or build the absolute URL for the resource and generate a SHA-1 hash.
7.  Generate a SHA-1 hash of the resource content.
8.  Generate a custom HTML5 attribute/value pair and inject it into the tag in the response data.
    * The custom attribute is in the format of `data-resource-sha1="[URL hash]:[content hash]"`, where `[URL hash]` is the hash generated in (6) and `[content hash]` is the hash generated in (7).  These values will be parsed by the client and used to determine whether the resource has changed.
9.  Write the response data to `stdout`.
    * Apache will return the response to the client.

*Note: Apache sets several environment variables, which are then used by the script to retrieve information about the request context, namely the request scheme (i.e. HTTP or HTTPS), host name (base URL) and document root (in the file system).  These values allow the script to translate resource URLs to file system paths so that it can generate the SHA-1 hash of the resource content.*

### Website<a name="site"></a>
The website provided in the demo is merely a set of static content designed to test different scenarios.  The main page, `index.html`, provides tags the link to resources both on the local server and on remote servers.  Additonally, it uses absolute and relative links to test operation of the injector script.  Finally, the provided JavaScript file parses the caching hints on the client-side and displays them on the page to allow verification that the server-side script and client-side proxy calculate the same hash.