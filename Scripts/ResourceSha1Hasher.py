#!/usr/bin/python
import commands
import os
import sys
import hashlib

##### Module variables #####
attrName = 'data-resource-sha1'
embeddedTags = {
    'script': 'src',
    'link': 'href',
    'img': 'src',
    'iframe': 'src',
    'embed': 'src',
    'object': 'data',
    'video': 'src',
    'audio': 'src',
    'source': 'src',
    'track': 'src',
    'area': 'href'
}
response = sys.stdin.read()

##### Functions #####
# Gets the value for the specified attribute in the specified tag.
def getAttributeValue(tag, attribute):
    attrIndex = tag.find(attribute)
    attrValue = None

    if attrIndex != -1 and attrIndex < len(tag) - 1:
        valStart = -1
        valEnd = -1

        # Start where the attribute name was found and find the pairs of quotes containing the value.
        for i in range(attrIndex + len(attribute), len(tag)):
            if tag[i] == '"' or tag[i] == '\'':
                if valStart == -1:
                    valStart = i + 1
                else:
                    valEnd = i
                    break  # no need to continue evaluating

        if valStart != -1 and valEnd != -1:
            attrValue = tag[valStart:valEnd]

    return attrValue

# Gets the relative path (from the document root) of the requested resource.  Returns None
# if the resource is located on a different server.
def getResourceRelativePath(resourceUrl):
    path = None
    protocolIndex = resourceUrl.find('://')

    # Check whether reference is relative or absolute.
    if protocolIndex != -1:  # Reference is absolute.
        resourceUrl = resourceUrl[protocolIndex + 3:]  # Strip off protocol including '://'
        pathStartIndex = resourceUrl.find('/')  # TODO: check for rest of path after the host name.
        host = resourceUrl[:pathStartIndex]

        # Make sure resource is on this server.
        if host == os.environ['HTTP_HOST']:
            path = resourceUrl[pathStartIndex:]
    else:
        # If path is relative to the document root, return it.
        if resourceUrl.startswith('/'):
            path = resourceUrl
        else:
            # Break down the paths as lists so we can manage the subdirectories.
            pageRelPath = os.environ['SCRIPT_NAME'][1:].split('/')
            pageRelPath.pop()  # Remove the document name.
            resourceUrl = resourceUrl.split('/')
            newUrl = list()

            # TODO: make sure we don't traverse above the document root.
            for dir in resourceUrl:
                if dir == '..':
                    pageRelPath.pop()
                elif dir != '.':
                    newUrl.append(dir)

            path = '/' + '/'.join(pageRelPath + newUrl)

    return path

# Searches the specified document content and returns a list of all tags that reference
# external content.
def getExtReferenceTags(content):
    contentLength = len(content)
    candidates = list()
    currentTag = None

    # Get the tags that potentially contain external content.
    for i in range(0, contentLength):
        # Start capturing on '<' and stop capturing on '>'.
        if response[i] == '<':
            # Ignore closing tags and !DOCTYPE declarations.
            if i < contentLength - 1 and response[i + 1] != '/' and response[i + 1] != '!':
                currentTag = dict()
                currentTag['start'] = i
        elif response[i] == '>' and currentTag:
            currentTag['end'] = i
            tagValue = response[currentTag['start'] + 1:currentTag['end']].strip()

            # Ignore any tags that aren't capable of referencing external content.
            for key in embeddedTags:
                if tagValue.startswith(key):
                    currentTag['tag'] = key
                    currentTag['value'] = '<' + tagValue + '>'
                    candidates.append(currentTag)

            currentTag = None

    tags = list()

    # Find the tags that actually reference external content.
    for tag in candidates:
        attr = embeddedTags[tag['tag']]
        resource = getAttributeValue(tag['value'], attr)

        if resource:
            tags.insert(0, tag)

    return tags

# Injects the custom attribute between the specified index values.
def injectHashAttribute(value, tagStart, tagStop):
    global response, attrName
    # TODO: self-closing tags can be broken if there is no space before the closing /.  E.g <img ... alt="my image"/>
    index = response.rfind(' ', tagStart, tagStop)

    if index != 1:
        response = response[:index] + ' ' + attrName + '="' + value + '"' + response[index:]

##### Script start #####
if __name__ == '__main__':
    scheme = os.environ['REQUEST_SCHEME']
    hostRoot = os.environ['HTTP_HOST']
    docRoot = os.environ['DOCUMENT_ROOT']
    toHash = getExtReferenceTags(response)

    # Tags are stored in reverse order of how they appear in the document so that
    # inserting a string won't affect subsequent tags.
    for tag in toHash:
        ref = getAttributeValue(tag['value'], embeddedTags[tag['tag']])
        resourcePath = getResourceRelativePath(ref)

        if resourcePath:
            resourceUri = scheme + '://' + hostRoot + resourcePath
            resourcePath = docRoot + resourcePath


            # Hash the file and URI and return the hashed values separated by a colon.  Client
            # will parse the string to extract the values.
            hasher = hashlib.sha1()
            with open(resourcePath, 'rb') as hashFile:
                buf = hashFile.read()
                hasher.update(buf)

            sha1sum = str(hasher.hexdigest())
            uriSha1sum = str(hashlib.sha1(resourceUri).hexdigest())

            injectHashAttribute(uriSha1sum + ':' + sha1sum, tag['start'], tag['end'])

    sys.stdout.write(response)